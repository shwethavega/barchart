import { Component, OnInit } from '@angular/core';
import * as Chart from 'chart.js'

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit {
title='barchart';
  barChart: any;
  constructor() { }

  ngOnInit() {
    this.barChart = new Chart('barChart',{
        type: 'bar',
        data: {
          labels: ["RED", "BLUE", "YELLOW","GREEN","PURPLE","ORANGE"],
          datasets: [{
            label: '# of Votes',
            data: [1,2,3,10,6,45],
            backgroundColor: [
              'rgba(255, 0, 0, 1.0)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 255, 0, 1.0)',
              'rgba(0, 128, 0, 1.0)',
              'rgba(128, 102, 128, 1.0)',
              'rgba(255,165,0, 2.0)'
            ],
            borderColor:[
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(255, 159, 64, 0.2)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          title: {
            text:"Bar Chart",
            display: true
          },
          scales:{
            yAxes:[{
              ticks:{
                beginAtZero:true
              }
            }]
          }
        }
      });

    }
  }

